import React, { Component } from 'react'
import ReactFullpage from '@fullpage/react-fullpage'
import Section from '@components/Section'
import ControlInfo from '@components/ControlInfo'

import intro from '@slides/intro'
import title from '@slides/title'
import why from '@slides/why'
import units from '@slides/units'
import selectors from '@slides/selectors'
import pseudoElements from '@slides/pseudoElements'
// import backgrounds from '@slides/backgrounds'
import boxSizing from '@slides/boxSizing'
import flexbox from '@slides/flexbox'
import grid from '@slides/grid'
import transitions from '@slides/transitions'
import animations from '@slides/animations'
import extras from '@slides/extras'
import questions from '@slides/questions'

import './App.scss'

class App extends Component {
  render() {

    const sections = [
      title,
      why,
      units,
      selectors,
      pseudoElements,
      // backgrounds,
      boxSizing,
      flexbox,
      grid,
      transitions,
      animations,
      extras,
      questions,
    ]

    const sectionTitles = [
      '',
      '',
      'Units',
      'Selectors',
      'Pseudo-Elements',
      // 'Backgrounds',
      'Box Sizing',
      'Flexbox',
      'CSS Grid',
      'Transitions',
      'Animations',
      'Extras',
      ''
    ]

    if (window.location.href.indexOf('localhost') > -1) {
      sections.unshift(intro)
      sectionTitles.unshift('')
    }

    return (
      <ReactFullpage
        scrollingSpeed={500}
        loopHorizontal={false}
        resetSliders
        controlArrows={false}
        slidesNavigation
        navigation
        navigationPosition='left'
        scrollOverflow
        navigationTooltips={sectionTitles}
        render={() => {
          return (
            <ReactFullpage.Wrapper>
              <ControlInfo />
              {sections.map((section) => {
                return <Section slides={section} />
              })}
            </ReactFullpage.Wrapper>
          )
        }}
      />
    )
  }
}

export default App;
