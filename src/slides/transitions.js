export default [
  {
    appearance: 'title',
    heading: 'Transitions'
  },
  {
    heading: 'Little touches',
    content: `
      <p>Transitions are micro, reversible animations that excite the user to interact with particular elements.</p>
      <p>They are quite simply a way of animating something from point-A to point-B and should be used for interactions within applications.</p>
    `
  },
  {
    heading: 'From A to B',
    content: `
      <p>The <code>transition</code> property is a shorthand property which takes in four longhand properties - <code>transition-property</code>, <code>transition-duration</code>, <code>transition-timing-function</code> & <code>transition-delay</code>.</p>
      <p>Transitioning properties allows the change to take place over a period of time, rather than immediately.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.element {
  &:hover {
    background-color: green;
  }
}`
      },
      {
        code: `.element {
  transition: background-color 0.5s ease;

  &:hover {
    background-color: green;
  }
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .no-transition:hover {
              background-color: green;
            }
          </style>
          <div class="example-wrapper">
            <div class="no-transition"></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .simple-transition {
              transition: background-color 0.5s ease;
            }
            .simple-transition:hover {
              background-color: green;
            }
          </style>
          <div class="example-wrapper">
            <div class="simple-transition"></div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Multiple transitions',
    content: `
      <p>You can run multiple transitions on a single element using the <code>all</code> property. You can also run transitions simultaneously by comma separating the values</p>
    `,
  },
  {
    codeSnippets: [
        {
        code: `.element {
  transition: all 0.5s ease;

  &:hover {
    background-color: green;
    width: 200px;
  }
}`
        },
        {
        code: `.element { // Run concurrently
  transition: background 0.5s ease,
              width 2s ease;

  &:hover {
    background-color: green;
    width: 200px;
  }
}`
        },
        {
        code: `.element { Stagger
  transition: background 0.5s ease,
              width 2s ease 0.5s;

  &:hover {
    background-color: green;
    width: 200px;
  }
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .simple-transition-2 {
              transition: all 0.5s ease;
            }
            .simple-transition-2:hover {
              background-color: green;
              width: 200px;
            }
          </style>
          <div class="example-wrapper">
            <div class="simple-transition-2"></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .simple-transition-3 {
              transition: background 0.5s ease, width 2s ease;
            }
            .simple-transition-3:hover {
              background-color: green;
              width: 200px;
            }
          </style>
          <div class="example-wrapper">
            <div class="simple-transition-3"></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .simple-transition-4 {
              transition: background 0.5s ease, width 2s ease 0.5s;
            }
            .simple-transition-4:hover {
              background-color: green;
              width: 200px;
            }
          </style>
          <div class="example-wrapper">
            <div class="simple-transition-4"></div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Easy does it',
    content: `
      <p>Where the <code>transition</code> sits is key to clean animations. Putting the property on the element itself will lead to cleaner transitions than adding the property to the <code>:hover</code> state.</p>
    `,
  },
  {
    codeSnippets: [
      {
      code: `.element {
  transition: all 0.5s ease;

  &:hover {
    background-color: green;
    width: 200px;
  }
}`
      },
      {
      code: `.element {
  &:hover {
    background-color: green;
    width: 200px;
    transition: all 0.5s ease;
  }
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .simple-transition-5 {
              transition: all 0.5s ease;
            }
            .simple-transition-5:hover {
              background-color: green;
              width: 200px;
            }
          </style>
          <div class="example-wrapper">
            <div class="simple-transition-5"></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .simple-transition-6:hover {
              transition: all 0.5s ease;
              background-color: green;
              width: 200px;
            }
          </style>
          <div class="example-wrapper">
            <div class="simple-transition-6"></div>
          </div>
        `
      },
    ]
  },
  {
    appearance: 'title',
    heading: 'Exercise',
    content: `
      <p>Head over to <a href="https://codepen.io/bennyhudson/pen/gObyPor" target="_blank">https://codepen.io/bennyhudson/pen/gObyPor</a></p>
      <p>When you hover over the button, the following should happen:</p>
      <ul>
        <li>The shadow should disappear over 500ms</li>
        <li>The arrow should move 10px to the right over 300ms - The button should not get any bigger</li>
        <li>The button colour should change to #2ecc71 over 300ms</li>
      </ul>
    `
  },
  {
    appearance: 'title',
    heading: 'Keep Learning',
    content: `
      <p>The definitive guide to transitions can be found on <a href="https://css-tricks.com/almanac/properties/t/transition/" target="_blank">CSS Tricks</a></p>
    `
  }
]
