export default [
  {
    appearance: 'title',
    heading: 'Welcome',
    content: `
      <p>To get started:</p>
      <ul>
        <li>Head to wedo-css-workshop.herokuapp.com</li>
        <li>Go fullscreen</li>
        <li>Hide your toolbar (cmd+shift+F)</li>
      </ul>
    `
  }
]
