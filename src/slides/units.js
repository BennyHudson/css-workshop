export default [
  {
    appearance: 'title',
    heading: 'Units'
  },
  {
    heading: 'So many units, so little time',
    content: `
      <p>Percentages, pixels, ems, rems, vws, chs... It feels like every week, there's a new unit of measurement being introduced to the CSS language.</p>
      <p>So what should you use, when should you use them and what do they actually do?</p>
    `,
  },
  {
    heading: 'Pixels',
    content: `
      <p>A pixel is the most basic method of sizing an element in CSS.</p>
      <p>When we size an element with pixels, we can be fairly sure how big it will be on the page.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `element {
  width: 100px;
  height: 100px;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper pixel-box">
            <div></div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Percentages',
    content: `
      <p>Percentage units take their values from their parent DOM element.</p>
      <p>Widths are easily calculated, providing the parent element is a block element. Heights can only take a value if the parent has a defined height.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `element {
  width: 100%;
  height: 100%;
}`
      },
      {
        code: `parent {
  height: 100px;
  width: 100px;
}
element {
  width: 100%;
  height: 100%;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .percentage-box-1 {
              width: 100% !important;
              height: 100% !important;
              background: #fe8c00;
            }
          </style>
          <div class="example-wrapper">
            <div class="percentage-box-1"></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .percentage-box-parent {
              width: 100px;
              height: 100px;
              background: none !important;
            }
            .percentage-box-2 {
              width: 100%;
              height: 100%;
              background: #fe8c00;
            }
          </style>
          <div class="example-wrapper">
            <div class="percentage-box-parent">
              <div class='percentage-box-2'></div>
            </div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'em Units',
    content: `
      <p>An <code>em</code> is a unit of measurement, equal to the currently specified point size. The name of <code>em</code> is related to M.</p>
      <p>Originally the unit was derived from the width of the capital “M” in the given typeface. Nowadays the <code>em</code> unit generally refers to the point size of the font.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `element {
  font-size: 20px;
  width: 10em;
  height: 10em;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .em-box-1 {
              font-size: 20px !important;
              width: 5em !important;
              height: 5em !important;
            }
          </style>
          <div class="example-wrapper">
            <div class='em-box-1'><div></div></div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'rem Units',
    content: `
      <p>A <code>rem</code> is similar to an <code>em</code> unit, the primary difference being that the value is taken from the root element - i.e. the html.</p>
      <p>The main advantage of <code>rem</code>s over <code>em</code>s is that they are always set based on the same value, nested <code>em</code> units can get messy.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `html {
  font-size: 62.5%; // This will set the base font size to 10px
}
element {
  width: 10rem;
  height: 10rem;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper">
            <div></div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'vh & vw Units',
    content: `
      <p><code>vh</code> - viewport height - and <code>vw</code> - viewport width -  units are the first truly responsive length units in the sense that their value changes every time the browser resizes.</p>
      <p><code>1vh</code> is equal to 1% of the viewport height, and <code>1vw</code> is equal to 1% of the viewport width.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `element {
  width: 5vw;
  height: 5vw;
}`
      },
      {
        code: `element {
  width: 5vh;
  height: 5vh;
}`
      },
      {
  code: `element {
  width: 5vw;
  height: 5vh;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .vh-box-1 {
              width: 5vw !important;
              height: 5vw !important;
            }
          </style>
          <div class="example-wrapper">
            <div class='vh-box-1'></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .vh-box-2 {
              width: 5vh !important;
              height: 5vh !important;
            }
          </style>
          <div class="example-wrapper">
            <div class='vh-box-2'></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .vh-box-3 {
              width: 5vw !important;
              height: 5vh !important;
            }
          </style>
          <div class="example-wrapper">
            <div class='vh-box-3'></div>
          </div>
        `
      }
    ]
  },
  {
    appearance: 'title',
    heading: 'Keep learning...',
    content: `<p>More information about units can be found on <a href="https://www.w3schools.com/cssref/css_units.asp">W3 Schools</a></p>`
  }
]
