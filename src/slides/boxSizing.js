export default [
  {
    appearance: 'title',
    heading: 'Box Sizing'
  },
  {
    heading: 'When is 100 pixels not 100 pixels?',
    content: `
      <p>Believe me when I say that the box sizing model was an absolute game changer in CSS. No longer did we have to do crazy calculations to get the padding <em>just</em> right.</p>
      <p>Box sizing gives us the consistency we need to build bulletproof layouts.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.box-1 {
  width: 300px;
  padding: 20px;
  border: 10px solid #000;
}`
      },
      {
        code: `.box-2 {
  width: 50%;
  padding: 20px;
  border: 10px solid #000;
}`
      },
    ]
  },
  {
    codeSnippets: [
      {
        code: `.box-1 {
  border-box: box-sizing;
  width: 300px;
  padding: 20px;
  border: 10px solid #000;
}`
      },
      {
        code: `.box-2 {
  border-box: box-sizing;
  width: 50%;
  padding: 20px;
  border: 10px solid #000;
}`
      },
    ]
  },
  {
    heading: 'Understanding the box sizing model',
    content: `
      <p>By default, <code>box-sizing</code> takes on the value of <code>content-box</code>. In this model, dimensions are calculated before padding and borders. This means that padding can drastically affect how wide you were expecting a box to be.</p>
      <p>In the pre-responsive world, this wasn't a problem. You knew how much space you had to play with, so you sized your box to fit. When responsive design came in, percentage values being used for widths became more prevalent, which made this sizing method obsolete.</p>
      <p>Step forward, <code>border-box</code>, which calculates the entire width of the box together with borders and padding, so now your <code>300px</code> box will actually be <code>300px</code> wide.</p>
    `
  }
]
