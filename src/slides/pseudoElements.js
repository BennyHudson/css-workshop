export default [
  {
    appearance: 'title',
    heading: 'Pseudo-Elements'
  },
  {
    heading: 'Pure CSS Elements',
    content: `
      <p>Sometimes we need more than what the markup can give us. We want to keep our code clean and semantic, but we also want visual flourishes that used to have to be made with nested divs in nested divs in nested...</p>
      <p>It used to be a nightmare, then CSS pseudo-elements arrived, and suddenly we could just create one element and then let the pseudo-elements handle the rest.</p>
    `,
  },
  {
    heading: 'The big two',
    content: `
      <p>Not to be confused with pseudo-selectors, pseudo-elements are specific pieces of code that can be added & styled with CSS.</p>
      <p>The most commonly used are the <code>::before</code> & the <code>::after</code> pseudo-elements. These can insert content before or after an html element.</p>
      <p>CSS pseudo-elements should be used with a double colon to stylistically distinguish them from pseudo-selectors.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Each before/after element has to have a content property
p::before {
  content: 'hello ';
  color: red;
}
p::after {
  content: ' goodbye';
  color: red;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .pseudo-example::before {
              content: 'hello ';
              color: red;
            }
            .pseudo-example::after {
              content: ' goodbye';
              color: red;
            }
          </style>
          <p class="pseudo-example">This is the paragraph</p>
        `
      }
    ]
  },
  {
    appearance: 'title',
    heading: 'Exercise',
    content: `
      <p>Using the following CodePen, without touching the html, add a telephone & chevron icon to this button.</p>
      <p><a href="https://codepen.io/bennyhudson/pen/xxbyMGP" target="_blank">https://codepen.io/bennyhudson/pen/xxbyMGP</a></p>
    `
  },
  {
    heading: 'Typography',
    content: `
      <p>A popular use for pseudo-elements is in typography. Using the <code>::first-line</code> or <code>::first-letter</code> pseudo-elements, some interesting typographical styling can be done.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `p::first-letter {
  color: red;
}
p::first-line {
  color: blue;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .pseudo-example-2::first-letter {
              color: red;
            }
            .pseudo-example-2::first-line {
              color: blue;
            }
          </style>
          <p class="pseudo-example-2">This is the paragraph.<br />It goes over two lines.</p>
        `
      }
    ]
  },
  {
    appearance: 'title',
    heading: 'Keep learning...',
    content: `<p>More information about pseudo-elements can be found on <a href="https://www.w3schools.com/cssref/css_units.asp">W3 Schools</a></p>`
  }
]
