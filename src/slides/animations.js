export default [
  {
    appearance: 'title',
    heading: 'Animations'
  },
  {
    heading: 'Can you just make it pop?',
    content: `
      <p>Animations turn a static UI into something that feels alive, adding interest and contributing to the design language of the application.</p>
      <p>Animation used to be the domain of JavaScript, but these days, most animations can now be handled in CSS.</p>
    `
  },
  {
    heading: 'Keyframes',
    content: `
      <p>The core of animations is keyframes. It's a video animation term that literally means 'during this frame, do something' - and then that frame becomes a 'key' frame.</p>
      <p>In CSS, keyframes work slightly differently, in that they're done in percentages (or using <code>from</code> and <code>to</code>), and defined separately to the timings. In CSS, you say 'after this percentage of the animation is complete, do something'.</p>
      <p>Keyframes are defined in CSS using the <code>@keyframes</code> declaration, in a similar manner to mixins being defined.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Define a simple keyframe set
@keyframes animation-name {
  0% { // Starting point of the animation
    background-color: red;
  }
  100% { // End point of the animation
    background-color: blue;
  }
}`
      },
      {
        code: `// Define a looping keyframe set
@keyframes animation-name {
  0%, 100% {
    background-color: red;
  }
  50% { // Mid point of the animation
    background-color: blue;
  }
}`
      },
    ]
  },
  {
    codeSnippets: [
      {
        code: `// Define a simple keyframe set
@keyframes move-left-to-right {
  0% { // Starting point of the animation
    left: 0%;
    transform: translateX(0);
  }
  100% { // End point of the animation
    left: 100%;
    transform: translateX(-100%);
  }
}`
      }
    ]
  },
  {
    heading: 'Making it move',
    content: `
      <p>Once your <code>@keyframes</code> are set up, you can start animating.</p>
      <p>The most basic format is to combine CSS properties into a singular shorthand.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.element {
  animation: animation-name 2s infinite;
}`
      },
      {
        code: `.element {
  animation: move-left-to-right 5s infinite alternate;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .simple-animated-box {
              animation: simple-animation-name 2s infinite;
            }
            .simple-animated-box-2 {
              animation: simple-animation-name 2s infinite;
            }

            @keyframes simple-animation-name {
              0%, 100% {
                background-color: red;
              }
              50% {
                background-color: blue;
              }
            }

            @keyframes move-left-to-right {
              0% {
                left: 0%;
                transform: translateX(0);
              }
              100% {
                left: 100%;
                transform: translateX(-100%);
              }
            }
          </style>
          <div class="example-wrapper">
            <div class="simple-animated-box"></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .simple-animated-box {
              animation: simple-animation-name 2s infinite;
            }
            .simple-animated-box-2 {
              position: absolute;
              top: 0;
              animation: move-left-to-right 5s alternate infinite ease;
            }
          </style>
          <div class="example-wrapper" style="position: relative; display: block; height: 110px;">
            <div class="simple-animated-box-2"></div>
          </div>
        `
      },
    ]
  },
  {
    heading: 'Deep dive',
    codeSnippets: [
      {
        code: `animation-name: // declares the name of the @keyframes at-rule to manipulate.
animation-duration: // the length of time it takes for an animation to complete one cycle.
animation-timing-function: // establishes preset acceleration curves such as ease or linear.
animation-delay: // the time between the element being loaded and the start of the animation sequence.
animation-direction: // sets the direction of the animation after the cycle. Its default resets on each cycle.
animation-iteration-count: // the number of times the animation should be performed.
animation-fill-mode: // sets which values are applied before/after the animation.
animation-play-state: // pause/play the animation.`
      }
    ]
  },
  {
    heading: 'Deep dive II',
    codeSnippets: [
      {
        code: `animation-duration: Xms | Xs
animation-timing-function: ease | ease-out | ease-in | ease-in-out | linear | cubic-bezier(x1, y1, x2, y2)
animation-delay: Xms | Xs
animation-direction: normal | alternate
animation-iteration-count: X | infinite
animation-fill-mode: forwards | backwards | both | none
animation-play-state: paused | running`
      }
    ]
  },
  {
    heading: 'Timing Functions',
    codeSnippets: [
      {
        code: `.element { // linear animation style
  animation-timing-function: linear;
}`
      },
      {
        code: `.element { // ease animation style
  animation-timing-function: ease;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .simple-animated-box-3 {
              position: absolute;
              top: 0;
              animation: move-left-to-right 5s linear infinite alternate;
            }
            .simple-animated-box-4 {
              position: absolute;
              top: 0;
              animation: move-left-to-right 5s ease infinite alternate;
            }
          </style>
          <div class="example-wrapper" style="position: relative; display: block; height: 110px;">
            <div class="simple-animated-box-3"></div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="position: relative; display: block; height: 110px;">
            <div class="simple-animated-box-4"></div>
          </div>
        `
      },
    ]
  },
  {
    heading: 'Multiple Animations',
    content: `<p>You can comma-separate the values to declare multiple animations on a selector as well.</p>`,
  },
  {
    codeSnippets: [
      {
        code: `.element {
  animation:
    animation-name 2s infinite,
    move-left-to-right 5s infinite alternate;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .simple-animated-box-5 {
              position: absolute;
              top: 0;
              animation: move-left-to-right 5s linear infinite alternate, simple-animation-name 2s infinite;
            }
          </style>
          <div class="example-wrapper" style="position: relative; display: block; height: 110px;">
            <div class="simple-animated-box-5"></div>
          </div>
        `
      }
    ]
  },
  {
    appearance: 'title',
    heading: 'Exercise',
    content: `
      <p>Using the CodePen, move the red box to the bottom right of the container element when the button is clicked. As it's moving, turn it green.</p>
      <p><a href="https://codepen.io/bennyhudson/pen/YzPgmrz" target="_blank">https://codepen.io/bennyhudson/pen/YzPgmrz</a></p>
    `
  },
  {
    appearance: 'title',
    heading: 'Keep Learning',
    content: `
      <p>As always, CSS Tricks has everything you need to know about <a href="https://css-tricks.com/almanac/properties/a/animation/" target="_blank">animations</a> and <a href="https://css-tricks.com/snippets/css/keyframe-animation-syntax/" target="_blank">keyframes</a>.</p>
      <p>MDN has a <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_animated_properties" target="_blank">list of CSS properties that can be animated</a>.</p>
      <p>Daniel Eden has taken the time to create <a href="https://daneden.github.io/animate.css/" target="_blank">all these animations</a>, so you don't have to.</p>
      <p>Otherwise, speak to our local animation expert, Brynley.</p>
    `
  }
]
