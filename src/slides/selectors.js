export default [
  {
    appearance: 'title',
    heading: 'Selectors'
  },
  {
    heading: 'Pick your target',
    content: `
      <p>Being able to write beautiful, clean CSS is great, but if you can't properly target the elements on the page, then all your hard work will be going to waste.</p>
      <p>In this section, we're going to look at how to target elements properly, how hierarchy works and how your markup can drastically alter how your CSS is implemented.</p>
    `,
  },
  {
    heading: 'Elements & Wildcards',
    content: `
      <p>At the bottom level of the CSS hierarchy, CSS can target <em>everything</em> with a wildcard selector. This is useful for things like resets, or when you don't know which element will be rendered.</p>
      <p>The next level of hierarchy is targeting each element. This is useful for base styles, giving each element a default style for when elements are rendered without any other properties.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Select every element
* {
  padding: 0;
  margin: 0;
}`
      },
      {
        code: `// Select every h1
h1 {
  padding: 20px;
  margin: 10px;
}`
      }
    ]
  },
  {
    heading: 'Classes',
    content: `
      <p>Elements can also be targeted using classes. This can also be made more specific by targeting specific html elements with a class.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Select every element with a class
.class {
  background: red;
  padding: 20px;
}
// Select multiple classes
.class-one,
.class-two {
  background: red;
}`
      },
      {
        code: `// Select every h1 with a class
.class {
  background: red;
  padding: 20px;
}
h1.class {
  background: green;
}`
      },
      {
        code: `// Select every h1 within a class
.class {
  background: red;
  padding: 20px;
}
.class h1 {
  background: green;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .class-selector-box-1 {
              background: red !important;
            }
          </style>
          <div class="example-wrapper">
            <div class='class-selector-box-1'></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .class-selector-box-2 {
              background: red;
              padding: 20px;
              width: auto !important;
              height: auto !important;
            }
            h1.class-selector-box-2 {
              background: green;
              font-size: 30px;
              line-height: 34px;
              color: #fff;
            }
          </style>
          <div class="example-wrapper">
            <div class='class-selector-box-2'>
              <h1 class='class-selector-box-2'>Heading</h1>
            </div>
          </div>
        `
      },
      {
        code: `
        <style>
          .class-selector-box-3 {
            background: red;
            padding: 20px;
            width: auto !important;
            height: auto !important;
          }
          .class-selector-box-3 h1 {
            background: green;
            font-size: 30px;
            line-height: 34px;
            color: #fff;
          }
        </style>
        <div class="example-wrapper">
          <div class='class-selector-box-3'>
            <h1>Heading</h1>
          </div>
        </div>
        `
      },
    ]
  },
  {
    heading: 'Pseudo-selectors',
    content: `
      <p>Not to be confused with pseudo-elements (more on that later), pseudo-selectors are additional attributes that can be used to target elements in certain scenarios.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Change the styling of a link on hover
a {
  color: red;
}
a:hover {
  color: blue;
}`
      },
      {
        code: `// Change an input on focus
input {
  border: 5px solid grey;
}
input:focus {
  border: 5px solid blue;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <style>
            .pseudo-selector-box-1 a {
              color: red;
            }
            .pseudo-selector-box-1 a:hover {
              color: blue;
            }
          </style>
          <div class='pseudo-selector-box-1'><a href='#'>Link</a></div>
        `
      },
      {
        code: `
          <style>
          .pseudo-selector-box-2 input {
            border: 5px solid grey;
            margin: 0;
          }
          .pseudo-selector-box-2 input:focus {
            border: 5px solid blue;
          }
          </style>
          <div class='pseudo-selector-box-2'>
            <input type='text' />
          </div>
        `
      }
    ]
  },
  {
    heading: 'Pseudo-selectors II',
    content: `
      <p>Pseudo-selectors can also be used to target specific elements, using <code>nth-child</code> values.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Target first and last children
.class {
  background: red;
}
.class:first-child {
  background: blue;
}
.class:last-child {
  background: green;
}`
      },
      {
        code: `// Target the only child
.class {
  background: red;
}
.class:only-child {
  background: green;
}`
      },

    ],
    htmlExamples: [
      {
        code: `
          <style>
            .pseudo-class-1 {
              float: left;
              margin-right: 10px;
              width: 50px !important;
              height: 50px !important;
            }
            .pseudo-class-1:first-child {
              background: blue;
            }
            .pseudo-class-1:last-child {
              background: green;
            }
          </style>
          <div class="example-wrapper">
            <div class='pseudo-class-1'></div>
            <div class='pseudo-class-1'></div>
            <div class='pseudo-class-1'></div>
            <div class='pseudo-class-1'></div>
            <div class='pseudo-class-1'></div>
            <div class='pseudo-class-1'></div>
            <div class='pseudo-class-1'></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .pseudo-class-1 {
              float: left;
              margin-right: 10px;
              width: 50px !important;
              height: 50px !important;
            }
            .pseudo-class-1:only-child {
              background: green;
            }
          </style>
          <div class="example-wrapper">
            <div class='pseudo-class-1'></div>
          </div>
        `
      },
    ]
  },
  {
    heading: 'Pseudo-selectors III',
  },
  {
    codeSnippets: [
      {
        code: `// Target just the third child
.class {
  background: red;
}
.class:nth-child(3) {
  background: blue;
}`
      },
      {
        code: `// Target every fourth child
.class {
  background: red;
}
.class:nth-child(4n) {
  background: blue;
}
.class:nth-child(4n+1) {
  background: green;
}`
      }

    ],
    htmlExamples: [
      {
        code: `
          <style>
            .pseudo-class-2 {
              float: left;
              margin-right: 10px;
              width: 50px !important;
              height: 50px !important;
            }
            .pseudo-class-2:nth-child(3) {
              background: blue;
            }
          </style>
          <div class="example-wrapper">
            <div class='pseudo-class-2'></div>
            <div class='pseudo-class-2'></div>
            <div class='pseudo-class-2'></div>
            <div class='pseudo-class-2'></div>
            <div class='pseudo-class-2'></div>
            <div class='pseudo-class-2'></div>
            <div class='pseudo-class-2'></div>
          </div>
        `
      },
      {
        code: `
          <style>
            .pseudo-class-3 {
              float: left;
              margin-right: 10px;
              width: 50px !important;
              height: 50px !important;
            }
            .pseudo-class-3:nth-child(4n) {
              background: blue;
            }
            .pseudo-class-3:nth-child(4n+1) {
              background: green;
            }
          </style>
          <div class="example-wrapper">
            <div class='pseudo-class-3'></div>
            <div class='pseudo-class-3'></div>
            <div class='pseudo-class-3'></div>
            <div class='pseudo-class-3'></div>
            <div class='pseudo-class-3'></div>
            <div class='pseudo-class-3'></div>
            <div class='pseudo-class-3'></div>
          </div>
        `
      },
    ]
  },
  {
    appearance: 'title',
    heading: 'Exercise',
    content: `
      <p>Without amending the HTML, arrange the squares into a 3x3 grid.</p>
      <p><a href="https://codepen.io/bennyhudson/pen/MWYqJXd" target="_blank">https://codepen.io/bennyhudson/pen/MWYqJXd</a></p>
      <p>Once the grid has been made, please make every other box black, so an X is formed.</p>
      <p>Bonus marks given if you can also remove the margin from the bottom row.</p>
    `
  },
  {
    heading: 'Attributes',
    content: `
      <p>Elements can also be selected with attributes, such as a data value or type. You can also select elements that have attributes that contain certain values.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Style text input differently to email input
input {
  border: 5px solid green;
}
input[type='text'] {
  border: 5px solid red;
}
input[type='email'] {
  border: 5px solid blue;
}`
      },
      {
        code: `// Target all classes that contain a string
div[class*='text'] {
  border: 5px solid red;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
        <style>
        .attribute-box-1 input {
          border: 5px solid green;
        }
        .attribute-box-1 input[type='text'] {
          border: 5px solid red;
        }
        .attribute-box-1 input[type='email'] {
          border: 5px solid blue;
        }
        </style>
        <div class="example-wrapper attribute-box-1" style="display: block;">
          <input type='number' />
          <input type='text' />
          <input type='email' />
        </div>
        `
      },
      {
        code: `
          <style>
            .attribute-box-2 div {
              float: left;
              margin-right: 10px;
              width: 50px !important;
              height: 50px !important;
            }
            .attribute-box-2 div[class*='text'] {
              border: 5px solid blue;
            }
          </style>
          <div class="example-wrapper attribute-box-2">
            <div></div>
            <div class="text-1"></div>
            <div></div>
            <div class="text-2"></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'IDs',
    content: `
      <p>At the top of the selector hierarchy is the ID. Conventional wisdom states that you should never style an item using an ID, so don't. But if you <em>really</em> have to... here's how.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Style an ID
#element {
  border: 5px solid green;
}`
      },
      {
        code: `// Use an ID as an override
.class {
  border: 5px solid red;
}
.class#id {
  border: 5px solid green;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
        <style>
          .id-wrapper-1 #element {
            border: 5px solid green;
            padding: 20px;
          }
        </style>
        <div class="example-wrapper id-wrapper-1">
          <div id="element"></div>
        </div>
        `
      },
      {
        code: `
        <style>
          .id-wrapper-2 .box {
            border: 5px solid blue;
            width: 50px !important;
            height: 50px !important;
            margin-right: 10px;
          }
          .id-wrapper-2 .box#id {
            border: 5px solid green;
          }
        </style>
          <div class="example-wrapper id-wrapper-2">
            <div class="box"></div>
            <div class="box"></div>
            <div class="box" id="id"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Blocks, Elements & Modifiers',
    content: `
      <p>BEM syntax has become the industry standard for working with a large codebase. It splits DOM elements into different categories... blocks, elements & modifiers.</p>
      <p>Blocks are the outer wrapper, like a header. Elements are things within that container, such as a menu. Modifiers are things that are added to blocks or elements to slightly change the appearance of them.</p>
      <p>It's used to ensure that classes are specific to the elements that they're being used for, and don't affect other elements.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Block
<div class="header"></div>
.header {
  border: 5px solid green;
}`
      },
      {
        code: `// Element
<div class="header">
  <div class="header__menu"></div>
</div>
.header__menu {
  border: 5px solid red;
}
`
      },
      {
        code: `// modifiers
<div class="header header--alt">
  <div class="header__menu header__menu--alt"></div>
</div>
`
      }
    ]
  },
  {
    heading: 'Additional CSS operators',
    content: `
      <p>Even with BEM syntax, we can't always - or sometimes we don't want to - add a class to every element. Sometimes classes can still get away from us, and nested elements can take on styles that nobody ever expected.</p>
      <p>Thankfully, CSS has an answer for us... operators like <code>></code> or <code>+</code> or <code>~</code> can be used to our advantage to seek out elements and style them without having to edit the markup.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `// Style the direct descendents
ul.list > li {
  border: 5px solid green;
}
<ul class="list">
  <li>I will be styled</li>
  <li>
    <ul>
      <li>I won't</li>
    </ul>
  </li>
</ul>
`
      },
      {
        code: `// Style the next element
div + p {
  border: 5px solid green;
}
<div></div>
<p>I will be styled</p>
<p>I won't</p>
`
      },
      {
        code: `// Style all the next elements
div ~ p {
  border: 5px solid green;
}
<div></div>
<p>I will be styled</p>
<p>So will I!</p>
`
      }
    ]
  },
  {
    appearance: 'title',
    heading: 'Keep learning...',
    content: `
      <p>More information about selectors can be found on <a href="https://www.w3schools.com/cssref/css_selectors.asp" target="_blank">W3 Schools</a></p>
      <p>You can learn more about BEM syntax <a href="http://getbem.com/introduction/" target="_blank">here</a></p>
      <p>Want to understand the cascade? <a href="https://wattenberger.com/blog/css-cascade" target="_blank">This is a great read</a></p>
    `
  }
]
