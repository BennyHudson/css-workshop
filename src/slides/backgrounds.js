export default [
  {
    appearance: 'title',
    heading: 'Backgrounds'
  },
  {
    heading: 'A wolf in sheeps clothing',
    content: `
      <p>Backgrounds? Why do we need a section on backgrounds?</p>
      <p>We can all write <code>background: #ff0000;</code> that's easy. But do you know how pixel density can affect an image? Do you know why your content editor is moaning because that image isn't cropping how they expected? Is your UX designer saying that the angle of that gradient is wrong? And how come that SVG looks dreadful in IE?</p>
    `,
  },
  {
    heading: 'Background Size',
    codeSnippets: [
      {
        code: `.element {
  background: url('');
}`
      },
      {
        code: `.element {
  background: url('');
  background-size: cover;
}`
      },
      {
        code: `.element {
  background: url('');
  background-size: contain;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper">
            <div style="width: 300px; height: 300px; background: url('https://source.unsplash.com/dUeD4im5YSQ/2000x1200') top left no-repeat;"
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper">
            <div style="width: 300px; height: 300px; background: url('https://source.unsplash.com/dUeD4im5YSQ/2000x1200') top left no-repeat; background-size: cover;"
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper">
            <div style="width: 300px; height: 300px; background: url('https://source.unsplash.com/dUeD4im5YSQ/2000x1200') top left no-repeat; background-size: contain;"
          </div>
        `
      }
    ]
  }
]
