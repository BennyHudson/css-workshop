export default [
  {
    appearance: 'title',
    heading: 'Flexbox'
  },
  {
    heading: 'The gamechanger',
    content: `
      <p>CSS can be split into two distinct eras. Before Flexbox (BFB) and After Flexbox (AFB). BFB were dark times. Content behaved in unpredictable ways, the old joke about vertically centring something in css <em>actually</em> made sense.</p>
      <p>At first, AFB were confusing times, a mess of polyfills (cheers IE) and vendor prefixes (cheers Firefox). It was still fairly unpredictable, but it was clearly the way forward.</p>
      <p>As browser support grew, and vendor prefixes were dropped, suddenly, developers & UI designers could actually achieve the same visions.</p>
      <p>Flexbox is a single-direction layout concept. Think of flex items as primarily laying out either in horizontal rows or vertical columns.</p>
    `,
  },
  {
    heading: 'Horizontal spacing',
    content: `
      <p>Most common flexbox properties apply to the parent element. Horizontal spacing is handled by the <code>justify-content</code> property.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.parent {
  display: flex;
  justify-content: flex-start;
}`
      },
      {
        code: `.parent {
  display: flex;
  justify-content: flex-end;
}`
      },
      {
        code: `.parent {
  display: flex;
  justify-content: center;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper">
            <div>1</div>
            <div style="width: 150px;">2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="justify-content: flex-end;">
            <div>1</div>
            <div style="width: 150px;">2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="justify-content: center;">
            <div>1</div>
            <div style="width: 150px;">2</div>
            <div>3</div>
          </div>
        `
      }
    ]
  },
  {
    codeSnippets: [
      {
        code: `.parent {
  display: flex;
  justify-content: space-between;
}`
      },
      {
        code: `.parent {
  display: flex;
  justify-content: space-around;
}`
      },
      {
        code: `.parent {
  display: flex;
  justify-content: space-evenly;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper" style="justify-content: space-between;">
            <div>1</div>
            <div style="width: 150px;">2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="justify-content: space-around;">
            <div>1</div>
            <div style="width: 150px;">2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="justify-content: space-evenly;">
            <div>1</div>
            <div style="width: 150px;">2</div>
            <div>3</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Vertical spacing',
    content: `
      <p>Most common flexbox properties apply to the parent element. Vertical spacing is handled by the <code>align-items</code> property.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.parent {
  display: flex;
  align-items: flex-start;
}`
      },
      {
        code: `.parent {
  display: flex;
  align-items: flex-end;
}`
      },
      {
        code: `.parent {
  display: flex;
  align-items: center;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper" style="height: 200px;">
            <div>1</div>
            <div style="height: 150px;">2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="height: 200px; align-items: flex-end;">
            <div>1</div>
            <div style="height: 150px;">2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="height: 200px; align-items: center;">
            <div>1</div>
            <div style="height: 150px;">2</div>
            <div>3</div>
          </div>
        `
      }
    ]
  },
  {
    codeSnippets: [
      {
        code: `.parent {
  display: flex;
  align-items: stretch;
}`
      },
      {
        code: `.parent {
  display: flex;
  align-items: baseline;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper" style="height: 200px; align-items: stretch;">
            <div style="height: auto;">1</div>
            <div style="height: auto;">2</div>
            <div style="height: auto;">3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="height: 200px; align-items: baseline;">
            <div>1</div>
            <div style="height: 150px;">2</div>
            <div>3</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Wrapping',
    content: `
      <p>By default, child items will not wrap, and will instead shrink to fit inside the container. By using the <code>flex-wrap</code> property, we can allow content onto more than one row.</p>
      <p><code>flex-wrap</code> can also be used alongside the <code>align-content</code> property to align a flex container's lines within when there is extra space in the cross-axis, similar to how <code>justify-content</code> aligns individual items within the main-axis.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.parent {
  display: flex;
  flex-wrap: nowrap;
}`
      },
      {
        code: `.parent {
  display: flex;
  flex-wrap: wrap;
}`
      },
      {
        code: `.parent {
  display: flex;
  flex-wrap: wrap-reverse;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper">
            <div style="width:100%;">1</div>
            <div style="width:100%;">2</div>
            <div style="width:100%;">3</div>
            <div style="width:100%;">4</div>
            <div style="width:100%;">5</div>
            <div style="width:100%;">6</div>
            <div style="width:100%;">7</div>
            <div style="width:100%;">8</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="flex-wrap: wrap;">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
            <div>5</div>
            <div>6</div>
            <div>7</div>
            <div>8</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="flex-wrap: wrap-reverse;">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
            <div>5</div>
            <div>6</div>
            <div>7</div>
            <div>8</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Direction',
    content: `
      <p>Flexbox can also change the direction of the content, using the <code>flex-direction</code> property. This is useful for responsive layouts.</p>
      <p>When reversing the direction of the elements, you don't reverse the order in the DOM. This means that screen readers will see the content in the order in which it appears, and may not read the content as you are expecting. Also, <code>first-child</code> and <code>last-child</code> selectors are not reversed with the content.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.parent {
  display: flex;
  flex-direction: row;
}`
      },
      {
        code: `.parent {
  display: flex;
  flex-direction: row-reverse;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper" style="flex-direction: row;">
            <div>1</div>
            <div>2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="flex-direction: row-reverse;">
            <div>1</div>
            <div>2</div>
            <div>3</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Direction II',
    content: `
      <p>Flexbox can also display content in a vertical column.</p>
      <p>This can have ramifications on other flexbox properties. For instance <code>justify-content</code> now applies to the vertical spacing, and <code>align-items</code> will apply to the horizontal spacing.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.parent {
  display: flex;
  flex-direction: column;
}`
      },
      {
        code: `.parent {
  display: flex;
  flex-direction: column-reverse;
  align-items: flex-end;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper" style="flex-direction: column;">
            <div>1</div>
            <div>2</div>
            <div>3</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper" style="flex-direction: column-reverse; align-items: flex-end;">
            <div>1</div>
            <div>2</div>
            <div>3</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Child styles - Vertical Spacing',
    content: `
      <p>While most flexbox properties apply to the parent element, child elements can also have their own flexbox styling.</p>
      <p>It is important to note that in order for a child element to use a flexbox property, it does not need to have <code>display: flex;</code> in its CSS.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.child--end {
  align-self: flex-end;
}`
      },
      {
        code: `.child--middle {
  align-self: center;
}`
      },
      {
        code: `.child--stretch {
  align-self: stretch;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper" style="justify-content: center; height: 200px;">
            <div>1</div>
            <div style="align-self: flex-end;">2</div>
            <div style="align-self: center;">3</div>
            <div style="align-self: stretch; height: auto;">4</div>
            <div>5</div>
            <div style="align-self: center;">6</div>
            <div>7</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Child styles - Horizontal Spacing',
    content: `
      <p>BFB, we used to use <code>float</code>s to position elements left and right. It worked, to a point, but it involved an awful lot of nested divs.</p>
      <p>When Flexbox came in, we were able to get rid of all the nested divs, and we could have proper semantic markup. But what if you want your wrapper to have <code>justify-content: flex-start;</code> but that last child needs to be on the far right?</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.child--end {
  margin-left: auto;
}`
      }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
            <div style="margin-left: auto;">5</div>
          </div>
        `
      },
      {
        code: `
          <div class="example-wrapper">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div style="margin-left: auto;">4</div>
            <div>5</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Growing & Shrinking',
    content: `
      <p>Flex elements can grow to fit the horizontal width of their container using the <code>flex-grow</code> property.</p>
      <p>If all items have <code>flex-grow</code> set to <code>1</code>, the remaining space in the container will be distributed equally to all children. If one of the children has a value of <code>2</code>, the remaining space would take up twice as much space as the others (or it will try to, at least).</p>
      <p>Conversely, <code>flex-shrink</code> defines the ability for a child item to shrink if necessary. A <code>0</code> value here will mean that the item <em>cannot</em> change size.</p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.stretch-item {
  flex-grow: 1;
}`
      },
      {
        code: `.fixed-width-item {
  flex-shrink: 0;
}`
      },
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper">
            <div style="flex-grow: 1;">1</div>
            <div style="flex-shrink: 0;">2</div>
          </div>
        `
      }
    ]
  },
  {
    heading: 'Nesting',
    content: `
      <p>Don't forget, flexbox can be nested. Because the values are defined by the parent, each child can have different flexbox values to pass down to the next level.<p>
    `,
  },
  {
    codeSnippets: [
      {
        code: `.flex-parent {
  display: flex;
  align-items: stretch;
  padding: 20px;
}`
      },
      {
        code: `.flex-parent-level-2 {
  display: flex;
  align-items: center;
  background: green;
  padding: 20px;
}`
    }
    ],
    htmlExamples: [
      {
        code: `
          <div class="example-wrapper" style="justify-content: center; height: auto;">
            <div style="height: auto; padding: 20px; width: 300px; align-items: stretch;">
              <div style="display: flex; align-items: center; background: green; height: auto; padding: 20px;">
                <h4>Short element</h4>
              </div>
              <div style="display: flex; align-items: center; background: green; height: auto; padding: 20px;">
                <h4>Much taller element that will still be centred properly.</h4>
              </div>
            </div>
          </div>
        `
      }
    ]
  },
  {
    appearance: 'title',
    heading: 'Exercise',
    content: `
      <p>Without amending the HTML, turn this:</p>
      <p><a href="https://codepen.io/bennyhudson/pen/rNaQNXE" target="_blank">https://codepen.io/bennyhudson/pen/rNaQNXE</a></p>
      <p>Into this:</p>
      <p><a href="https://codepen.io/bennyhudson/full/PowxwqE" target="_blank">https://codepen.io/bennyhudson/full/PowxwqE</a></p>
    `
  },
  {
    appearance: 'title',
    heading: 'Keep learning...',
    content: `<p>There's no better guide to Flexbox out there than Chris Coyier's on <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">CSS Tricks</a>.</p>
    <p>Read it, bookmark it, print it out and set it as your homepage.</p>`
  }
]
