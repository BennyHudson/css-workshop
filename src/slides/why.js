export default [
  {
    appearance: 'simple',
    heading: 'Why?',
    content: `
      <blockquote>
        <p>CSS is a fundamental language imperative to working with websites. It controls all the visual aspects of a site, from colors to layouts.</p>
        <p>Even the most experienced developer can learn something new when it comes to using and understanding how the browser interprets CSS.</p>
        <p>- Tyler Clark, egghead.io</p>
      </blockquote>
    `
  }
]
