export default [
  {
    appearance: 'title',
    heading: 'Further Reading'
  },
  {
    heading: 'We haven\'t got all day...',
    content: `
      <p>...so here are some things to look into in your own time.</p>
      <ul style="display: flex; flex-wrap: wrap;">
        <li>Background Gradients</li>
        <li>Clipping Paths</li>
        <li>CSS Grid</li>
        <li>Box Shadows</li>
        <li>Text Shadows</li>
        <li>RGBA colours</li>
        <li>HSA colours</li>
        <li>Border Radius</li>
        <li>Fallbacks</li>
        <li>CSS Columns</li>
        <li>Shapes</li>
        <li>Background Sizing</li>
        <li>Letter Spacing</li>
        <li>Word Spacing</li>
        <li>Blend Modes</li>
        <li>Transforms</li>
      </ul>
    `
  },
  {
    heading: 'Who to follow',
    content: `
      <p>I would say me, (<a target="_blank" href="https://twitter.com/bennyhudson">@BennyHudson</a>, if you're asking), but I very rarely (AKA never) tweet about CSS. These people do though:</p>
      <ul style="display: flex; flex-wrap: wrap;">
        <li><a target="_blank" href="https://twitter.com/rachelandrew">Rachel Andrew</a></li>
        <li><a target="_blank" href="https://twitter.com/laras126">Lara Schenck</a></li>
        <li><a target="_blank" href="https://twitter.com/MinaMarkham">Mina Markham</a></li>
        <li><a target="_blank" href="https://twitter.com/jina">Jina Anne</a></li>
        <li><a target="_blank" href="https://twitter.com/tabatkins">Tab Atkins</a></li>
        <li><a target="_blank" href="https://twitter.com/mbarker_84">Michelle Barker</a></li>
        <li><a target="_blank" href="https://twitter.com/sonniesedge">Charlie Owen</a></li>
        <li><a target="_blank" href="https://twitter.com/brucel">Bruce Lawson</a></li>
        <li><a target="_blank" href="https://twitter.com/adactio">Jeremy Keith</a></li>
        <li><a target="_blank" href="https://twitter.com/jensimmons">Jen Simmons</a></li>
        <li><a target="_blank" href="https://twitter.com/robinrendle">Robin Rendle</a></li>
        <li><a target="_blank" href="https://twitter.com/chriscoyier">Chris Coyier</a></li>
        <li><a target="_blank" href="https://twitter.com/hankchizljaw">Andy Bell</a></li>
        <li><a target="_blank" href="https://twitter.com/Mandy_Kerr">Mandy Michael</a></li>
        <li><a target="_blank" href="https://twitter.com/davatron5000">Dave Rupert</a></li>
        <li><a target="_blank" href="https://twitter.com/SaraSoueidan">Sara Soueidan</a></li>
        <li><a target="_blank" href="https://twitter.com/beep">Ethan Marcotte</a></li>
        <li><a target="_blank" href="https://twitter.com/ireaderinokun">Ire Aderinokun</a></li>
        <li><a target="_blank" href="https://twitter.com/heydonworks">Heydon Pickering</a></li>
        <li><a target="_blank" href="https://twitter.com/meyerweb">Eric Meyer</a></li>
        <li><a target="_blank" href="https://twitter.com/brad_frost">Brad Frost</a></li>
        <li><a target="_blank" href="https://twitter.com/LeaVerou">Lea Verou</a></li>
        <li><a target="_blank" href="https://twitter.com/StuRobson">Stuart Robson</a></li>
      </ul>
    `
  }
]
