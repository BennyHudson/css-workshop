export default [
  {
    appearance: 'title',
    heading: 'CSS Grid'
  },
  {
    heading: 'Why grid isn\'t covered here',
    content: `
      <p>The elephant in the room today is CSS Grid. I'm not teaching it today, for a few reasons:</p>
      <ul>
        <li>I haven't taken the time to learn it myself</li>
        <li>I don't believe it's relevant to the work we do at Vodafone</li>
        <li>There are already fantastic resources out there to learn it</li>
        <li>There's so much in there that we simply don't have time</li>
      </ul>
    `
  },
  {
    appearance: 'title',
    heading: 'Keep/Start Learning',
    content: `
      <p>Chris Coyier has written <a href="https://css-tricks.com/snippets/css/complete-guide-grid/" target="_blank">a guide to CSS Grid</a>, talking through all the properties and how it all works together.</p>
      <p>For those of you that like videos, Wes Bos has a <a href="https://cssgrid.io/" target="_blank">CSS Grid course</a> which is completely free.</p>
    `
  }
]
