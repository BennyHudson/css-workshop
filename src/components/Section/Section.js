import React, { Component } from 'react'
import Slide from '@components/Slide'
import styles from './Section.scss'

class Section extends Component {
  render () {

    const sectionClasses = [
      'section',
      styles['section-background']
    ].join(' ')

    return (
      <div className={sectionClasses}>
        <div className={styles['section-content']}>
        {
          this.props.slides.map((slide) => {
            return (
              <Slide
                appearance={slide.appearance}
                heading={slide.heading}
                content={slide.content}
                codeSnippets={slide.codeSnippets}
                htmlExamples={slide.htmlExamples}
              />
            )
          })
        }
        </div>
      </div>
    )
  }
}

export default Section
