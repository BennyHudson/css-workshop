import React, { Component } from 'react'
import styles from './Render.scss'

class Render extends Component {
  render () {
    return (
      <div className={styles.render}>
        <div dangerouslySetInnerHTML={{ __html: this.props.code }} />
      </div>
    )
  }
}

export default Render
