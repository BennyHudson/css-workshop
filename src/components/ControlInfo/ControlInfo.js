import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './ControlInfo.scss'

class ControlInfo extends Component {
  render () {

    return (
      <div className={styles['control-info']}>
        <FontAwesomeIcon icon={['fal','keyboard']} size='2x' />
        <span>Use up/down keys to move between sections</span>
        <span>Use left/right keys to move between slides</span>
      </div>
    )
  }
}

export default ControlInfo
