import React, { Component } from 'react'
import styles from './Heading.scss'

class Heading extends Component {
  render () {
    const headingClasses = [
      styles['heading'],
      styles[`heading-${this.props.level}`]
    ].join(' ')

    if (this.props.level > 6) return null

    const Component = `h${this.props.level}`

    return (
      <Component className={headingClasses}>
        { this.props.children }
      </Component>
    )
  }
}

export default Heading
