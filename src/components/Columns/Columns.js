import React, { Component } from 'react'
import styles from './Columns.scss'

class Columns extends Component {
  render () {
    return (
      <div className={styles.columns}>
        { this.props.children }
      </div>
    )
  }
}

export default Columns
