import React, { Component } from 'react'
import Heading from '@components/Heading'
import Columns from '@components/Columns'
import Render from '@components/Render'

import styles from './Slide.scss'

class Slide extends Component {
  render () {
    return (
      <div className='slide'>
        <div className={styles['slide-content']}>
          {this.props.appearance === 'feature' && <Heading level={1}>{this.props.heading}</Heading>}
          {this.props.heading && this.props.appearance !== 'feature' &&
            <Heading level={this.props.appearance === 'title' ? 2 : 3}>{this.props.heading}</Heading>
          }
          {this.props.content &&
            <div dangerouslySetInnerHTML={{__html: this.props.content}} />
          }
          {(this.props.codeSnippets || this.props.htmlExamples) &&
            <div>
              {this.props.codeSnippets &&
                <Columns>
                  {this.props.codeSnippets.map((snippet) => {
                    return (
                      <aside>
                        <pre>{snippet.code}</pre>
                      </aside>
                    )
                  })}
                </Columns>
              }
              {this.props.htmlExamples &&
                <Columns>
                  {this.props.htmlExamples.map((example) => {
                    return (
                      <aside>
                        <Render code={example.code} />
                      </aside>
                    )
                  })}
                </Columns>
              }
            </div>
          }
        </div>
      </div>
    )
  }
}

export default Slide
